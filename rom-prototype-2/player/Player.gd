extends KinematicBody2D

const CONST = preload("res://Constants.gd")

export (int) var holding = CONST.RESOURCE.EMPTY

const ACCELERATION = 20
const MAX_SPEED = 200

#	Preload Scenes

var motion = Vector2(0.0,0.0)
var objectsInArea = []
var targetObject = null
var player_power = { "gather" : 1, "attack" : 1, "build" : 1}
var player_state = "state_move"

func _ready():
	pass

func _process(delta):
#	print(holding)
#	MOVEMENT ================
	if (Input.is_action_pressed("ui_right") || Input.is_action_pressed("ui_left") || Input.is_action_pressed("ui_up") || Input.is_action_pressed("ui_down")):
		if Input.is_action_pressed("ui_right"):
			motion.x = min(MAX_SPEED, motion.x + ACCELERATION);
			$Sprite.flip_h = true;
			$Sprite.get_children()[0].play("Run")
		if Input.is_action_pressed("ui_left"):
			motion.x = min(MAX_SPEED, motion.x - ACCELERATION);
			$Sprite.flip_h = false;
			$Sprite.get_children()[0].play("Run")
		if Input.is_action_pressed("ui_up"):
			motion.y = min(MAX_SPEED, motion.y - ACCELERATION);
			$Sprite.flip_h = true;
			$Sprite.get_children()[0].play("Run")
		if Input.is_action_pressed("ui_down"):
			motion.y = min(MAX_SPEED, motion.y + ACCELERATION);
			$Sprite.flip_h = false;
			$Sprite.get_children()[0].play("Run")
		targetObject = lookForClosestObject()
	else :
		$Sprite.get_children()[0].play("Idle")
	move_and_slide(motion)
	motion.x = lerp(motion.x, 0, 0.1)
	motion.y = lerp(motion.y, 0, 0.1)
#	ACTION ================
	if (Input.is_action_just_pressed("ui_accept") && targetObject != null):
		print("INTERACT")
		interact(targetObject)
	if (Input.is_action_pressed("ui_accept") && targetObject != null):
#		print("WHOA PLAYER ACTUALLY HOLD SPACE")
		interact_hold(targetObject, delta)
		

func interact(body):
	if(!is_instance_valid(body)):
		return
#	if (body.is_in_group("resourceNode") && holding == CONST.RESOURCE.EMPTY):
#		var resource_type = body.call("withdraw",player_power.gather)
	elif (body.is_in_group("preStructure") && holding != CONST.RESOURCE.EMPTY):
		var right_resource = body.call("build",holding)
		if(right_resource):
			update_holding(CONST.RESOURCE.EMPTY)
		

func interact_hold(body, delta):
	if(!is_instance_valid(body)):
		return
	if (body.is_in_group("resourceNode") && holding == CONST.RESOURCE.EMPTY):
		var resource_type = body.call("mine",player_power.gather * delta)
		if (resource_type != null):
			update_holding(resource_type)

func gather(resource_node):
	var resource = resource_node.call("withdraw")
	update_holding(resource)
	return resource

func update_holding(resource):
	holding = resource
	if (holding == -1):
		for child in $Holding.get_children():
			child.queue_free()
		return
		
	var resourceScene = load(CONST.SCENE.RESOURCE_SCENE)
	var resourceNode = resourceScene.instance()
	resourceNode.call("init_with_resource",resource)
	$Holding.add_child(resourceNode)

func lookForClosestObject():
	if objectsInArea.size() > 0 :
		var closetObject = objectsInArea.front()
		for object in objectsInArea :
			if object.global_position.distance_to(global_position) <=  closetObject.global_position.distance_to(global_position) :
				closetObject = object
		return closetObject
	else :
		return null

func _on_Area2D_body_entered(body):
	objectsInArea.append(body)

func _on_Area2D_body_exited(body):
	objectsInArea.erase(body)
