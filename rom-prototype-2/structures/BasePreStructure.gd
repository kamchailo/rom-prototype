extends StaticBody2D

const CONST = preload("res://Constants.gd")

export (PackedScene) var complete_structure

onready var required_resource = $RequiredResources.required_resources
onready var structure = null
onready var stage = get_parent()


func build(resource):
	if (required_resource[resource] >= 1):
		required_resource[resource] -= 1
		if($RequiredResources.is_completed()):
			spawn_structure()
		return true
	else:
		return false

func spawn_structure():
	structure = complete_structure.instance()
	structure.position = position
	print("Add ",structure, " to ", stage)
	stage.add_child(structure)