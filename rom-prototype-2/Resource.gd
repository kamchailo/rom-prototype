extends Node2D

const CONST = preload("res://Constants.gd")

var resource_type

func set_resource(resource):
	if ( resource == -1 ):
		queue_free()
	else :
		$Sprite.frame = resource

func init_with_resource(resource):
	resource_type = resource
	set_resource(resource)
