extends Node2D

export (float) var max_progress = 32
var progress = 0 setget progress_set, progress_get

func _ready():
	progress = max_progress
	$Background.set_scale(Vector2(max_progress, 1.0))

func _process(delta):
	$Content.set_scale(Vector2(progress, 1.0))

func progress_set(value):
	progress = value * 32
	
func progress_get():
	return progress