const DIR = {
	"RESOURCE_DIR": "res://resources/"
}

const SCENE = {
	"RESOURCE_SCENE" : DIR.RESOURCE_DIR + "Resource.tscn"
}

const RESOURCE_NAME = [
	"EMPTY",
	"WOOD",
	"STONE",
	"COPPER_ORE",
	"TIN_ORE",
	"PLANK",
	"STONE_BLOCK",
	"SHARP_STONE",
	"COPPER_INGOT",
	"TIN_INGOT",
	"WOODEN_TOOL",
	"STONE_TOOL"
]

const RESOURCE = {
	"EMPTY" : -1,
	"WOOD" : 0,
	"STONE" : 1,
	"COPPER_ORE" : 2,
	"TIN_ORE" : 3,
	"PLANK" : 10,
	"STONE_BLOCK" : 11,
	"SHARP_STONE" : 12,
	"COPPER_INGOT" : 13,
	"TIN_INGOT" : 14,
	"WOODEN_TOOL" : 20,
	"STONE_TOOL" : 21
}

const RESOURCE_NUM = {
	-1 : "EMPTY",
	0 : "WOOD",
	1 : "STONE",
	2 : "COPPER_ORE",
	3 : "TIN_ORE",
	10 : "PLANK",
	11 : "STONE_BLOCK",
	12 : "SHARP_STONE",
	13 : "COPPER_INGOT",
	14 : "TIN_INGOT",
	20 : "WOODEN_TOOL",
	21 : "STONE_TOOL"
	}

const TIMER = {
	"MINE_TIMEOUT" : 1
}