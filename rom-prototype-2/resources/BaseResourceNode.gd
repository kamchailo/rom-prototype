extends StaticBody2D

const CONST = preload("res://Constants.gd")

export (String, "EMPTY","WOOD","STONE","COPPER_ORE","TIN_ORE") var resource_type
export (int) var deposit_amount
export (int) var power_required

onready var progress_bar = $ProgressBar
onready var mine_timer = $MineTimer

var resource
var amount
var current_power_required

func _ready():
	resource = CONST.RESOURCE[resource_type]
	amount = deposit_amount
	current_power_required = power_required

func _process(delta):
	if (mine_timer.get_time_left()<=0.1):
		current_power_required = power_required
		progress_bar.set_visible(false)

func withdraw(withdraw_amount):
	if (amount - withdraw_amount <= 0):
		destroy()
	amount -= withdraw_amount
	return resource

func mine(gather_power):
	progress_bar.set_visible(true)
	current_power_required -= gather_power
	mine_timer.start(CONST.TIMER.MINE_TIMEOUT)
	if(current_power_required <= 0):
		current_power_required = power_required
		progress_bar.set_visible(false)
		return withdraw(1)
	else:
		progress_bar.progress_set(current_power_required / power_required)
		return null


func destroy():
	queue_free()