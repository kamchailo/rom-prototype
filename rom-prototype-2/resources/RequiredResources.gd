extends Node

const CONST = preload("res://Constants.gd")


# RAW RESOURES
export (int) var WOOD = 0
export (int) var STONE = 0
export (int) var COPPER_ORE = 0
export (int) var TIN_ORE = 0

# REFINED RESOURCES
export (int) var PLANK = 0
export (int) var STONE_BLOCK = 0
export (int) var SHARP_STONE = 0
export (int) var COPPER_INGOT = 0
export (int) var TIN_INGOT = 0

# CRAFTED RESOURCES
export (int) var WOODEN_TOOL = 0
export (int) var STONE_TOOL = 0

var required_resources = {"EMPTY" : 0}

func _ready():
#	=== RAW RESOURCES ===
	required_resources [CONST.RESOURCE.WOOD] = WOOD
	required_resources [CONST.RESOURCE.STONE] = STONE
	required_resources [CONST.RESOURCE.COPPER_ORE] = COPPER_ORE
	required_resources [CONST.RESOURCE.TIN_ORE] = TIN_ORE
#	=== REFINED RESOURCES ===
	required_resources [CONST.RESOURCE.PLANK] = PLANK
	required_resources [CONST.RESOURCE.STONE_BLOCK] = STONE_BLOCK
	required_resources [CONST.RESOURCE.SHARP_STONE] = SHARP_STONE
	required_resources [CONST.RESOURCE.COPPER_INGOT] = COPPER_INGOT
	required_resources [CONST.RESOURCE.TIN_INGOT] = TIN_INGOT
#	=== CRAFTED RESOURCES ===
	required_resources [CONST.RESOURCE.WOODEN_TOOL] = WOODEN_TOOL
	required_resources [CONST.RESOURCE.STONE_TOOL] = STONE_TOOL

func is_completed():
	var count = 0
	for resource in required_resources :
		count += required_resources[resource]
	print ("COUNT ",count)
	if (count > 0 ):
		return false
	return true